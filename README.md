# ruGhost

![](https://gitlab.com/gnughost/design/-/raw/master/ruGhost.svg)

## Installation

```
$ pip install happytransformer
```

## Preparing Data

```
$ cd data
$ ./clone_top_ruby_projects.sh
```

```
$ find data -name \*.rb -print > all_ruby_files.txt
```

```
$ sed '/schema\.rb/d' all_ruby_files.txt > without.txt
```

```
rm all_ruby_files.txt
mv without.txt all_ruby_files.txt
```

## Improving The Model

```
$ python improvement.py
```

## Releases

* [https://archive.org/details/ruGhost_2022_09_28_07_17_IST](https://archive.org/details/ruGhost_2022_09_28_07_17_IST)
