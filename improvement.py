import random
import torch
from happytransformer import HappyGeneration
number_of_files_to_train = 5

def random_file(file_list):
    return random.choice(file_list).strip()

with open('all_ruby_files.txt') as f:
    file_list = f.readlines()


happy_gen = HappyGeneration(model_type="GPT-NEO", model_name="EleutherAI/gpt-neo-125M", load_path="ruGhost/")


random_files = []

for i in range(number_of_files_to_train):
    random_files.append(random_file(file_list))


for file_to_train in random_files:
    print(f"Training with: {file_to_train}")
    happy_gen.train(file_to_train)


happy_gen.save("ruGhost/")
